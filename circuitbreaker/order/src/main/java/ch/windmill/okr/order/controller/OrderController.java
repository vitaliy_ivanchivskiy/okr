package ch.windmill.okr.order.controller;

import ch.windmill.okr.order.dao.Order;
import ch.windmill.okr.order.dao.OrderRepository;
import com.github.javafaker.Faker;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.errors.NotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("orders")
public class OrderController {

  private static int DELAY = 500;
  private final Faker faker;

  private final OrderRepository orderRepository;

  @GetMapping
  List<Order> getCustomerOrders() {
    return orderRepository.findAll();
  }

  @GetMapping("/{id}")
  Order getOrder(@PathVariable("id") Integer id) {
    return orderRepository.findById(id);
  }

  @SneakyThrows
  @GetMapping("/{id}/error")
  Order getOrderWithError(@PathVariable("id") Integer id) {
    throw new NotSupportedException("Operation not supported");
  }

  @SneakyThrows
  @GetMapping("/{id}/error-50")
  Order getOrderWithRandomError(@PathVariable("id") Integer id) {
    var step = faker.random().nextInt(0, 50);
    if (step <= 25) {
      return orderRepository.findById(id);
    } else {
      throw new NotSupportedException("Operation not supported");
    }
  }

  @SneakyThrows
  @GetMapping("/{id}/slow")
  Order getOrderWithSlowResponse(@PathVariable("id") Integer id) {
    var durationMs = faker.random().nextInt(500, 5000);
    Thread.sleep(durationMs);
    log.info("Fetching order with delay {} ms", durationMs);
    return orderRepository.findById(id);
  }

}
