package ch.windmill.okr.order.dao;

import java.math.BigDecimal;
import java.util.List;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepository {

  public List<Order> findAll() {
    return getOrders();
  }

  public Order findById(@NonNull Integer id) {
    return getOrders()
        .stream()
        .filter(order -> id.equals(order.getId()))
        .findFirst()
        .orElse(null);
  }

  private List<Order> getOrders() {
    return List.of(
        Order.builder()
            .id(1)
            .amount(new BigDecimal("100"))
            .description("Bank Transfer Payment")
            .build(),
        Order.builder()
            .id(2)
            .amount(new BigDecimal("50"))
            .description("Gift Card Payment")
            .build(),
        Order.builder()
            .id(3)
            .amount(new BigDecimal("47.13"))
            .description("Cryptocurrency Payment")
            .build(),
        Order.builder()
            .id(4)
            .amount(new BigDecimal("13.56"))
            .description("Cash Payment")
            .build(),
        Order.builder()
            .id(5)
            .amount(new BigDecimal("2.48"))
            .description("Apple Pay Payment")
            .build()
    );
  }


}
