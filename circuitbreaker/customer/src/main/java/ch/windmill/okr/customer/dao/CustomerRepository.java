package ch.windmill.okr.customer.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository {

  public List<Customer> findAll() {
    return getCustomers();
  }

  public Optional<Customer> findById(@Nullable Integer customerId) {
    return Optional.ofNullable(customerId)
        .map(id -> getCustomers()
            .stream()
            .filter(customer -> id.equals(customer.getId()))
            .findFirst())
        .orElse(null);
  }

  private List<Customer> getCustomers() {
    return List.of(
        Customer.builder()
            .id(1)
            .name("John Doe")
            .orders(List.of(1,2,3))
            .build(),
        Customer.builder()
            .id(2)
            .name("Boba Fett")
            .orders(List.of(4,5))
            .build(),
        Customer.builder()
            .id(3)
            .name("Don Francks")
            .orders(List.of(1))
            .build()
    );
  }

}
