package ch.windmill.okr.customer.rest;

import ch.windmill.okr.customer.dao.Order;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
    url = "${integration.services.order}",
    dismiss404 = true,
    name = "order-service")
public interface OrderService {

  @GetMapping("/orders/")
  List<Order> getCustomerOrders();

  @GetMapping("/orders/{id}")
  Order getOrder(@PathVariable("id") Integer id);

  @GetMapping("/orders/{id}/error")
  Order getOrderWithError(@PathVariable("id") Integer id);

  @GetMapping("/orders/{id}/error-50")
  Order getOrderWithRandomError(@PathVariable("id") Integer id);

  @GetMapping("/orders/{id}/slow")
  Order getOrderWithSlowResponse(@PathVariable("id") Integer id);

}
