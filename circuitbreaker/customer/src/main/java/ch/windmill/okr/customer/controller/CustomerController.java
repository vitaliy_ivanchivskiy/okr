package ch.windmill.okr.customer.controller;

import ch.windmill.okr.customer.dao.Customer;
import ch.windmill.okr.customer.dao.CustomerRepository;
import ch.windmill.okr.customer.dao.Order;
import ch.windmill.okr.customer.exception.CustomerNotFoundException;
import ch.windmill.okr.customer.service.CustomerService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("customers")
public class CustomerController {

  private final CustomerRepository customerRepository;
  private final CustomerService customerService;

  @GetMapping
  List<Customer> getCustomers() {
    return customerRepository.findAll();
  }

  @GetMapping("/{id}")
  Customer getCustomer(@PathVariable("id") Integer id) {
    return customerRepository.findById(id)
        .orElseThrow(CustomerNotFoundException::new);
  }

  @GetMapping("/{id}/orders")
  Set<Order> getCustomerOrders(@PathVariable("id") Integer id) {
    return customerService.getCustomerOrders(id);
  }

  @GetMapping("/{id}/orders/error")
  Set<Order> getCustomerOrdersWithError(@PathVariable("id") Integer id) {
    return customerService.getOrdersWithError(id);
  }

  @GetMapping("/{id}/orders/error-50")
  Set<Order> getCustomerOrdersWithRandomError(@PathVariable("id") Integer id) {
    return customerService.getOrdersWithRandomError(id);
  }

  @GetMapping("/{id}/orders/slow")
  Set<Order> getCustomerOrdersWithSlowResponse(@PathVariable("id") Integer id) {
    return customerService.getOrdersWithSlowResponse(id);
  }

}
