package ch.windmill.okr.customer;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import io.github.resilience4j.timelimiter.TimeLimiterRegistry;
import java.time.Duration;
import java.util.Map;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Resilience4jConfig {

  public static final String CIRCUIT_BREAKER_CONFIG_ORDER_RANDOM_ERROR = "CIRCUIT_BREAKER_CONFIG_ORDER_RANDOM_ERROR";
  public static final String CIRCUIT_BREAKER_CONFIG_ORDER_ERROR = "CIRCUIT_BREAKER_CONFIG_ORDER_ERROR";
  public static final String CIRCUIT_BREAKER_CONFIG_ORDER_SLOW_CALL_DURATION = "CIRCUIT_BREAKER_CONFIG_ORDER_SLOW_CALL_DURATION";
  public static final int DURATION_IN_OPEN_STATE_SECONDS = 30;
  private static final int ALLOWABLE_WAITING_TIME_MILLIS = 2500;
  private static final int MINIMUM_NUMBER_OF_CALLS = 5;
  private static final int FAILURE_RATE_THRESHOLD_PERCENTAGE = 50;

  @Bean
  @Qualifier(CIRCUIT_BREAKER_CONFIG_ORDER_RANDOM_ERROR)
  public CircuitBreakerConfig orderRandomError() {
    return CircuitBreakerConfig.custom()
        .minimumNumberOfCalls(MINIMUM_NUMBER_OF_CALLS)
        .failureRateThreshold(FAILURE_RATE_THRESHOLD_PERCENTAGE)
        .waitDurationInOpenState(Duration.ofSeconds(DURATION_IN_OPEN_STATE_SECONDS))
        .writableStackTraceEnabled(false)
        .build();
  }

  @Bean
  @Qualifier(CIRCUIT_BREAKER_CONFIG_ORDER_SLOW_CALL_DURATION)
  public CircuitBreakerConfig orderSlowCallDuration() {
    return CircuitBreakerConfig.custom()
        .minimumNumberOfCalls(MINIMUM_NUMBER_OF_CALLS)
        .waitDurationInOpenState(Duration.ofSeconds(DURATION_IN_OPEN_STATE_SECONDS))
        .slowCallDurationThreshold(Duration.ofMillis(ALLOWABLE_WAITING_TIME_MILLIS))
        .writableStackTraceEnabled(false)
        .build();
  }

  @Bean
  @Qualifier(CIRCUIT_BREAKER_CONFIG_ORDER_ERROR)
  public CircuitBreakerConfig orderError() {
    return CircuitBreakerConfig.custom()
        .minimumNumberOfCalls(MINIMUM_NUMBER_OF_CALLS)
        .waitDurationInOpenState(Duration.ofSeconds(DURATION_IN_OPEN_STATE_SECONDS))
        .writableStackTraceEnabled(false)
        .build();
  }

  @Bean
  public CircuitBreakerRegistry circuitBreakerRegistry() {
    return CircuitBreakerRegistry.of(Map.of(
            CIRCUIT_BREAKER_CONFIG_ORDER_RANDOM_ERROR, orderRandomError(),
            CIRCUIT_BREAKER_CONFIG_ORDER_SLOW_CALL_DURATION, orderSlowCallDuration(),
            CIRCUIT_BREAKER_CONFIG_ORDER_ERROR, orderError()
        )
    );
  }

  @Bean
  TimeLimiterConfig timeLimiterConfig() {
    return TimeLimiterConfig.custom()
        .cancelRunningFuture(true)
        .timeoutDuration(Duration.ofMillis(ALLOWABLE_WAITING_TIME_MILLIS))
        .build();
  }

  @Bean
  TimeLimiterRegistry timeLimiterRegistry() {
    return TimeLimiterRegistry.of(timeLimiterConfig());
  }

}
