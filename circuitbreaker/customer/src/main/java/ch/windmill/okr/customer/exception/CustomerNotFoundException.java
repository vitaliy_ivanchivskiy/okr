package ch.windmill.okr.customer.exception;

public class CustomerNotFoundException extends RuntimeException {

  public CustomerNotFoundException() {
    super("Customer not found!");
  }
}
