package ch.windmill.okr.customer.service;

import static ch.windmill.okr.customer.Resilience4jConfig.CIRCUIT_BREAKER_CONFIG_ORDER_ERROR;
import static ch.windmill.okr.customer.Resilience4jConfig.CIRCUIT_BREAKER_CONFIG_ORDER_RANDOM_ERROR;
import static ch.windmill.okr.customer.Resilience4jConfig.CIRCUIT_BREAKER_CONFIG_ORDER_SLOW_CALL_DURATION;

import ch.windmill.okr.customer.dao.CustomerRepository;
import ch.windmill.okr.customer.dao.Order;
import ch.windmill.okr.customer.exception.CustomerNotFoundException;
import ch.windmill.okr.customer.rest.OrderService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {

  private final OrderService orderService;
  private final CustomerRepository customerRepository;
  private final CircuitBreakerFactory circuitBreakerFactory;

  public Set<Order> getCustomerOrders(Integer customerId) {
    var customer = customerRepository.findById(customerId)
        .orElseThrow(CustomerNotFoundException::new);
    return customer.getOrders()
        .stream()
        .map(this::fetchOrder)
        .collect(Collectors.toSet());
  }

  @CircuitBreaker(name = CIRCUIT_BREAKER_CONFIG_ORDER_RANDOM_ERROR, fallbackMethod = "getOrderFromAnotherService")
  public Set<Order> getOrdersWithRandomError(Integer customerId) {
    var customer = customerRepository.findById(customerId)
        .orElseThrow(CustomerNotFoundException::new);

    return customer.getOrders()
        .stream()
        .map(orderService::getOrderWithRandomError)
        .collect(Collectors.toSet());
  }

  public Set<Order> getOrdersWithSlowResponse(Integer customerId) {
    var customer = customerRepository.findById(customerId)
        .orElseThrow(CustomerNotFoundException::new);
    var circuitBreaker = circuitBreakerFactory.create(
        CIRCUIT_BREAKER_CONFIG_ORDER_SLOW_CALL_DURATION);

    return circuitBreaker.run(
        () -> customer.getOrders()
            .stream()
            .map(orderService::getOrderWithSlowResponse)
            .collect(Collectors.toSet()));
  }

  public Set<Order> getOrdersWithError(Integer customerId) {
    var customer = customerRepository.findById(customerId)
        .orElseThrow(CustomerNotFoundException::new);

    var circuitBreaker = circuitBreakerFactory.create(
        CIRCUIT_BREAKER_CONFIG_ORDER_ERROR);
    return circuitBreaker.run(() -> customer.getOrders()
        .stream()
        .map(orderService::getOrderWithError)
        .collect(Collectors.toSet()));
  }

  private Order fetchOrder(Integer orderId) {
    var order = orderService.getOrder(orderId);
    log.info("Fetched Order with ID: {}", orderId);
    return order;
  }

  private Set<Order> getOrderFromAnotherService(Integer customerId) {
    var orders = Set.of(Order.builder()
        .id(100)
        .amount(new BigDecimal("33.56"))
        .description("Default payment")
        .build());
    log.info("Fetched Order from another service");
    return orders;
  }

  private Set<Order> getOrderFromAnotherService(Integer customerId, Throwable throwable) {
    var orders = Set.of(Order.builder()
        .id(100)
        .amount(new BigDecimal("33.56"))
        .description("Default payment")
        .build());
    log.info("Fetched Order from another service");
    return orders;
  }

}
