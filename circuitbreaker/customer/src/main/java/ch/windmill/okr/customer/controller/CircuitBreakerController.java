package ch.windmill.okr.customer.controller;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreaker.Metrics;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import java.time.Duration;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("curcuit-breakers")
public class CircuitBreakerController {

  private final CircuitBreakerRegistry registry;

  @PostMapping("/{name}/open/{seconds}")
  public void transitToOpenState(@PathVariable String name, @PathVariable(name = "seconds") int seconds) {
    registry.circuitBreaker(name).transitionToOpenStateFor(Duration.ofSeconds(seconds));
  }

  @PostMapping("/{name}/close")
  public void transitToClosedState(@PathVariable String name) {
    registry.circuitBreaker(name).transitionToClosedState();
  }

  @PostMapping("/{name}/half-open")
  public void transitToHalfOpen(@PathVariable String name) {
    registry.circuitBreaker(name).transitionToHalfOpenState();
  }

  @GetMapping("/{name}/metrics")
  public CircuitBreaker.Metrics getMetrics(@PathVariable String name) {
    return registry.circuitBreaker(name).getMetrics();
  }

}
